BIN=can_udp
#TOOLCHAIN=/opt/mipsel-linux/bin/mipsel-linux-
#CC_PARAMS = -mips32r2 -Wall -O3
CC_PARAMS = -Wall -O3

SRCS = $(wildcard *.c)
OBJS = $(patsubst %.c, %.o, $(SRCS))

all: $(BIN)

$(BIN): $(OBJS)
	$(TOOLCHAIN)gcc $(CC_PARAMS) $(OBJS) -o $(BIN)

%.o: %.c Makefile
	$(TOOLCHAIN)gcc $(CC_PARAMS) -c -o $@ $<

clean:
	rm -f $(BIN) *.o

x86:
	make clean
	make all TOOLCHAIN= CC_PARAMS=

mipsel:
	make clean
	make all TOOLCHAIN="/opt/mipsel-linux/bin/mipsel-linux-" CC_PARAMS="-mips32r2 -Wall -O3"

upload:
	scp -i ~/plc_id_dsa $(BIN) root@217.71.42.47:/mnt/data/

