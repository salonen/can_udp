#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <time.h>
#include <sys/timerfd.h>
#include <fcntl.h>

#define MAX(a,b) (((a)>(b))?(a):(b))

#define BUFLEN 512
#define DEFAULT_PORT 19995
#define DEFAULT_CANIF "can0"
#define NET_KEEPALIVE 25

#define MODE_SERVER 0
#define MODE_CLIENT 1

#define DISCONNECTED 0
#define CONNECTED 1

int can_open_interface(char *interface);
void print_can(struct can_frame, uint8_t);
void die(char*);

uint8_t g_debug;
uint8_t g_auto_flow;	// auto answer with flow ctrl to multiline packets
uint8_t g_filter_ext;	// filter extended rows

#define FLOW_CMD (uint8_t[]){0x30,0x00,0x05,0xAA,0xAA,0xAA,0xAA,0xAA}

void usage(char *name){
	printf("%s [options] [remote ip] [remote port]\n", name);
	printf("-i interface (default %s)\n", DEFAULT_CANIF);
	printf("-r remote port (default %d)\n", DEFAULT_PORT);
	printf("-l listen port (default %d)\n", DEFAULT_PORT);
	printf("-a auto answer with flow ctrl to multiline packets\n");
	printf("-f filter extended packets on can interface\n");
	printf("-v verbose\n");
	printf("if ip is missing then act as server. default port is %d\n", DEFAULT_PORT);
//	exit(1);
}

int main(int argc, char *argv[]){
	struct sockaddr_in addr_local, addr_remote, addr_current;
	int c, i;
	struct timeval tv;
	int timerfd, net_sock, can_sock, max_sd;
	fd_set fds, fds_orig;
	struct itimerspec flow_timeout= {{0, 50*1000000}, {0, 0}}; // 50ms
	unsigned int slen=sizeof(addr_remote);
	uint8_t conn_status=DISCONNECTED;
	uint8_t net_mode=MODE_SERVER;
	uint16_t port=DEFAULT_PORT;
	char buf[BUFLEN];
	char ip_buffer[INET_ADDRSTRLEN];
	char *can_if=DEFAULT_CANIF;
	struct can_frame f;
	time_t t;	// keepalive timer
	canid_t last_remote_id=0;

	g_debug=g_auto_flow=g_filter_ext=0;

	while((c = getopt(argc, argv, "hi:dvp:r:l:af?-")) != -1){
		if(c=='l' || c=='r') port=atoi(optarg);
		else if(c=='i') can_if = optarg;
		else if(c=='d' || c=='v') g_debug=1;
		else if(c=='a') g_auto_flow=1;
		else if(c=='f') g_filter_ext=1;
		//else if(c=='?' || c=='h' || c=='-') usage(argv[0]);
	}
	usage(argv[0]);

	if((can_sock=can_open_interface(can_if)) < 0) die("can_open_interface");

	if(optind < argc && strlen(argv[optind])<INET_ADDRSTRLEN) { // there are more arguments
		memset(ip_buffer, 0, INET_ADDRSTRLEN);
		strncpy(ip_buffer, argv[optind], strlen(argv[optind]));
		if((optind+1)<argc) port=atoi(argv[optind+1]);
		net_mode=MODE_CLIENT;
		conn_status=CONNECTED; // client is always connected
		printf("mode: CLIENT, remote: %s:%d, %s\n", ip_buffer, port, can_if);
	} else {
		printf("mode: SERVER, port: %d, %s\n", port, can_if);
	}

	if((net_sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) die("socket");
	if(net_mode==MODE_SERVER){
		memset((char *) &addr_local, 0, sizeof(addr_local));
		addr_local.sin_family = AF_INET;
		addr_local.sin_port = htons(port);
		addr_local.sin_addr.s_addr = htonl(INADDR_ANY);
		if (bind(net_sock, (struct sockaddr *)&addr_local, sizeof(addr_local))==-1) die("bind");
	} else {
		memset((char *) &addr_remote, 0, sizeof(addr_remote));
		addr_remote.sin_family = AF_INET;
		addr_remote.sin_port = htons(port);
		inet_pton(AF_INET, ip_buffer, &addr_remote.sin_addr);
		if(sendto(net_sock, "", 0, 0, (struct sockaddr *)&addr_remote, sizeof(struct sockaddr_in))==-1) die("sendto()"); // send empty packet to inform server
	}

	time(&t);
	if((timerfd = timerfd_create(CLOCK_MONOTONIC, 0))<0) printf("error0\n");
	max_sd=MAX(timerfd, MAX(can_sock, net_sock));
	FD_SET(net_sock, &fds_orig);
	FD_SET(can_sock, &fds_orig);

	while(1){
		fds=fds_orig;
		tv = (struct timeval) { 1L, 0L };
		if((i=select(max_sd+1, &fds, NULL, NULL, &tv)) < 0) die("select error"); // block for 1sec
		if(i>0){
			// NET
			if (FD_ISSET(net_sock, &fds)) {
				if ((i=recvfrom(net_sock, buf, BUFLEN, 0, (struct sockaddr *)&addr_current, &slen)) < 0) printf("Socket error\n");
				else if(net_mode==MODE_SERVER && (memcmp(&(addr_current.sin_addr), &(addr_remote.sin_addr), sizeof(struct in_addr)) != 0 || addr_current.sin_port!=addr_remote.sin_port)){
					memcpy(&addr_remote, &addr_current, sizeof(struct sockaddr_in));
					conn_status=CONNECTED;
					printf("new connection %s:%d\n", inet_ntop(AF_INET, &((struct sockaddr_in *)&addr_current)->sin_addr, ip_buffer, INET_ADDRSTRLEN), addr_current.sin_port);
				}
				if(i>4){ // can_id[4]+data
					memcpy(&f.can_id, buf, 4); // assume host and remote endianness is same. if not, use ntohl/htonl
					f.can_dlc=i-4;
					memcpy(f.data, &buf[4], f.can_dlc);
					if(g_auto_flow){
						last_remote_id=f.can_id; // remember remote last id
						if(f.data[0]==0x30 && f.can_dlc==8){ // received flow ctrl
							if(FD_ISSET(timerfd, &fds_orig)) FD_CLR(timerfd, &fds_orig); // if timer running, forward packet and disable timer
							else { // discard packet
								print_can(f,'!');
								f.can_id=0;
							}
						}
						// lets try to delay our flow ctrl packet
						//// we must pass 0x10 and buffer others. when bus responds with 0x30, only then send 0x2x packets (from buffer)
						//if(f.data[0]==0x10){}
					}
					if(f.can_id){
						i=write(can_sock, &f, CAN_MTU);
						print_can(f,'<');
						if(i < 0) printf("ERROR writing CAN packet (%s)\n", strerror(errno));
					}
				} else {
					// probably keepalive
				}
			} // net_sock

			// CAN
			if (FD_ISSET(can_sock, &fds)) {
				if((i=read(can_sock, &f, CAN_MTU))<0) printf("CAN read error\n");
				if(conn_status==CONNECTED && i>0 && f.can_dlc>0 && !(g_filter_ext && f.can_id&CAN_EFF_FLAG)){ // discard extended packets
					//uint32_t x=ntohl(f.can_id);
					//memcpy(buf, &x, 4); // uint32_t
					memcpy(buf, &f.can_id, 4); // little-endian
					memcpy(&buf[4], f.data, f.can_dlc);
					// tulevikus allpool asuv funktsiooni parse_bus_packet(f);
					// if(g_auto_flow && f.data[0]=0x30){} // send flow ctrl. we don't know if remote also uses auto-flow
					if(sendto(net_sock, buf, 4+f.can_dlc, 0, (struct sockaddr *)&addr_remote, sizeof(struct sockaddr_in))<0) printf("can send error\n");
					else print_can(f, '>');
					if(g_auto_flow && (f.data[0]&0xF0)==0x10 && last_remote_id){ // sending multipacket. answer yourself with flow ctrl
						// set timer to 50ms. can be 0-127ms (https://www.sti-innsbruck.at/sites/default/files/courses/fileadmin/documents/vn-ws0809/03-vn-CAN-HLP.pdf)
						flow_timeout.it_value.tv_sec = 0;
						flow_timeout.it_value.tv_nsec = 50*1000000; // 50ms
						if(timerfd_settime(timerfd, 0, &flow_timeout, NULL)<0) printf("error: %s\n", strerror(errno));
						FD_SET(timerfd, &fds_orig);
					}
					time(&t);
				}
			} // can_sock

			if (FD_ISSET(timerfd, &fds)) { // lets send flow ctrl
				FD_CLR(timerfd, &fds_orig);
				f.can_id=last_remote_id;
				memcpy(f.data, FLOW_CMD, 8);
				write(can_sock, &f, CAN_MTU);
				print_can(f,'{');
				if(i < 0) printf("ERROR writing CAN packet (%s)\n", strerror(errno));
			}

		} // select()

		if(net_mode==MODE_CLIENT && (t+NET_KEEPALIVE)<time(NULL)){
			if(sendto(net_sock, "", 0, 0, (struct sockaddr *)&addr_remote, sizeof(struct sockaddr_in))==-1) die("sendto()");
			time(&t);
		}
	}

	close(net_sock);
	close(can_sock);
	return 0;
}

void print_can(struct can_frame f, uint8_t c){
	int i;
	if(!g_debug) return;
	printf((f.can_id<0x800?"%c %03X#":"%c %08X#"), c, f.can_id);
	for(i=0;i<f.can_dlc;i++) printf("%02X", f.data[i]);
	printf("\n");
}

void die(char *s){
	perror(s);
	exit(1);
}

int can_open_interface(char *interface){
	struct ifreq ifr;
	struct sockaddr_can addr;
	int can_fd;

	if((can_fd = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0){
		printf("socket: %s\n", strerror(errno));
		return(-1);
	}

	addr.can_family = AF_CAN;
	strcpy(ifr.ifr_name, interface);
	if(ioctl(can_fd, SIOCGIFINDEX, &ifr) < 0){
		printf("ioctl SIOCGIFINDEX: %s\n", strerror(errno));
		return(-1);
	}

	addr.can_ifindex = ifr.ifr_ifindex;
	if(bind(can_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0){
		printf("bind: %s\n", strerror(errno));
		return(-1);
	}
	return(can_fd);
}

